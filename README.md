# YamlConverter

Linux kernel development, porting hardware to Linux

This software converts older txt format files, where is hardware description to newer yaml format. See <code>src/main/resources</code> for test files


Written with intention to help porting hardware to linux



# Run
 First argument have to be directory, where are text files, to be converted to yaml

<code>
java -jar yamlConverter-0.0.1-SNAPSHOT.jar c:/windows/temp/yaml
</code>