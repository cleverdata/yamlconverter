package cz.cleverdata.yamlconverter.model;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Builder;
import lombok.Data;


@Builder
@Data
public class OutputFileContent {

    //information from json
    JsonNode jsonNode;

    //all original file here
    String description;

}
