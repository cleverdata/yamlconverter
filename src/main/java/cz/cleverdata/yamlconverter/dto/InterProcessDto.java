package cz.cleverdata.yamlconverter.dto;

import cz.cleverdata.yamlconverter.model.OutputFileContent;
import lombok.Data;

@Data
public class InterProcessDto {
    OutputFileContent outputFileContent;
    String originalFilename;
}
