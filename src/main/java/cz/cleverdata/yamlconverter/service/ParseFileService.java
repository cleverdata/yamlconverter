package cz.cleverdata.yamlconverter.service;

import com.fasterxml.jackson.databind.JsonNode;
import cz.cleverdata.yamlconverter.dto.InterProcessDto;
import cz.cleverdata.yamlconverter.model.OutputFileContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ParseFileService {
    @Autowired
    JsonMappingService jsonMappingService;

    @Autowired
    ExtractJsonService extractJsonService;

    public InterProcessDto parseFile(Path path) throws IOException {
        InterProcessDto interProcessDto = new InterProcessDto();
        interProcessDto.setOriginalFilename(path.toFile().toString());
        //parse json to yaml
        var jsonInText = extractJsonService.produceJsonInString(path);

        //sanitize txt input to be json
        JsonNode jsonNode = jsonMappingService.produceJson(jsonInText);
        interProcessDto.setOutputFileContent(OutputFileContent.builder().description(Files.readString(Paths.get(path.toFile().toString()), StandardCharsets.UTF_8)).jsonNode(jsonNode).build());
        //are there any more recognizible objects? Parse also
        //TODO: do it if necessary, but output would be strange


        return interProcessDto;

    }
}
