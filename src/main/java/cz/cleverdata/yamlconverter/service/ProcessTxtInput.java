//package cz.cleverdata.yamlconverter.service;
//
//import cz.cleverdata.yamlconverter.dto.InterProcessDto;
//import cz.cleverdata.yamlconverter.model.OutputFileContent;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//
//import java.io.IOException;
//import java.nio.file.Path;
//import java.util.ArrayList;
//import java.util.Scanner;
//
//@Slf4j
//@Service
//public class ProcessTxtInput {
//
//    public InterProcessDto parseYaml(Path path) throws RuntimeException {
//
//        var interProcessDto = new InterProcessDto();
//        var outputFileContent = new OutputFileContent();
//        interProcessDto.setOriginalFilename(path.toFile().toString());
//        try {
//            Scanner scan = new Scanner(path);
//            ArrayList<String> list = new ArrayList<>();
//            //create array, scanner is not good type for our purpose
//            while (scan.hasNext()) {
//                String line = scan.nextLine().toString();
//                list.add(line);
//            }
//
//            var lines = list.toArray(new String[list.size()]);
//            // first line is usually description
//            outputFileContent.setDescription(lines[0]);
//            for (int i = 1; i < lines.length; i++) {
//                if (lines[i].trim().length() < 4) {
//                    continue;
//                }
//                //read  properties
//                if (lines[i].trim().startsWith("-") && lines[i].contains(":")) {
//                    var splitLines = lines[i].trim().split(":");
//                    outputFileContent.getProperties().put(descriptionWithError(splitLines), splitLines[splitLines.length-1]);
//                }
//            }
//
//            interProcessDto.setOutputFileContent(outputFileContent);
//            return interProcessDto;
//        } catch (IOException e) {
//            log.error("", e);
////            e.printStackTrace();
//            return null;
//        }
//
//    }
//
//    /**
//     * if teher are multiple :, return all af this and mark ###ERROR###
//     * @param description
//     * @return
//     */
//    private String descriptionWithError(String[] description){
//        if(description.length>2) {
//            StringBuffer stringBuffer = new StringBuffer();
//            stringBuffer.append("###ERROR###");
//            for(int i=0;i<description.length-1;i++){
//                stringBuffer.append(description[i]);
//            }
//            return stringBuffer.toString();
//        } else {
//            return description[0];
//        }
//    }
//}
