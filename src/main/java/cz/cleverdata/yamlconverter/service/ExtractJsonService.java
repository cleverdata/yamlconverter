package cz.cleverdata.yamlconverter.service;

import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.Scanner;


@Service
public class ExtractJsonService {

    public String produceJsonInString(Path path) throws RuntimeException {

        try {
            StringBuffer stringBuffer = new StringBuffer();
            Boolean firstBracketFound = false;
            Boolean addLAstBracket = false;
            Scanner scan = new Scanner(path);
            while (scan.hasNext()) {
                String line = scan.nextLine();
                //replace strange characters by space " "
                line = line.replaceAll("[\f\n\r\t\'\"\\\\]", " ");
                //replace multiple spaces by one
                line = line.trim().replaceAll(" +", " ");
                //replace multiple newline by one
                line = line.replaceAll("\n+","\n");
                //search for line, where { bracket is present
                if (line.contains("{") && !firstBracketFound) {
                    //we have found the first bracket
                    firstBracketFound = true;
                    if (line.trim().substring(0, 1).equals("{")) {
                        stringBuffer.append(addColonAndquotationmark(line));
                    } else {
                        addLAstBracket = true;
                        stringBuffer.append("{" + addColonAndquotationmark(line));
                    }
                    stringBuffer.append(System.lineSeparator());
                    continue;
                }

                if (firstBracketFound) {
                    stringBuffer.append(addColonAndquotationmark(line));
                    stringBuffer.append(System.lineSeparator());
                    continue;
                }
            }

            String sanitizeCommas = stringBuffer.toString();

            if(addLAstBracket){
                sanitizeCommas = sanitizeCommas+"}";
            }


            //replace semicolons by none
            sanitizeCommas = sanitizeCommas.trim().replaceAll(";", "");

            //replace multiple spaces by none
            sanitizeCommas = sanitizeCommas.trim().replaceAll(" +", "");

            //replace multiple newline by none
            sanitizeCommas = sanitizeCommas.replaceAll(System.lineSeparator(),"");

            //at the end, remove ","
            sanitizeCommas = sanitizeCommas.replace(",}", "}");

            return sanitizeCommas;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String addColonAndquotationmark(String toFix) {
        // "missing":
        if (toFix.contains("{")) {
            String addColumnsAndQuitationMarks = toFix.substring(0, toFix.indexOf("{")).trim();
            addColumnsAndQuitationMarks = " \"" + addColumnsAndQuitationMarks + "\"" + ": ";
            return addColumnsAndQuitationMarks + toFix.substring(toFix.indexOf("{"));
        }
        var split = toFix.split("=");
        // "missing" = "missing";
        if (toFix.contains("=")) {

            if (!split[0].trim().startsWith("\"") && !split[0].trim().startsWith("\'")) {
                split[0] = "\"" + split[0].trim() + "\"";
            }
            // value has not " or contain multiples "
            if (!split[1].trim().startsWith("\"") && !split[1].trim().startsWith("\'") || split[1].split("\"").length > 2) {
                split[1] = "\"" + split[1].trim().replace("\"", "").replace("\\", "/") + "\"";
            }
            toFix = split[0] + ":" + split[1] + ",";
        }
        // missing "," after ;
        if (toFix.replace(" ", "").contains("};")) {
            toFix = toFix.replace(";", ",");
        }
        return toFix;
    }


}
