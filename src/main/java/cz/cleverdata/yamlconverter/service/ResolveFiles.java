package cz.cleverdata.yamlconverter.service;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ResolveFiles {

    public List<Path> getAllFilesInDirectory(String directory) throws IOException {
        Path start = Paths.get(directory);
        List<Path> collect;
        try (Stream<Path> stream = Files.walk(start, Integer.MAX_VALUE)) {
            collect = stream.filter(path -> (path.toFile().toString().endsWith("txt") && !path.toFile().isDirectory()))
                    .collect(Collectors.toList());
        }
        return collect;
    }
}
