package cz.cleverdata.yamlconverter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import cz.cleverdata.yamlconverter.dto.InterProcessDto;
import cz.cleverdata.yamlconverter.service.ParseFileService;
import cz.cleverdata.yamlconverter.service.ResolveFiles;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@SpringBootApplication
public class YamlConverterApplication implements CommandLineRunner {

    @Autowired
    ResolveFiles resolveFiles;

    @Autowired
    ParseFileService parseFileService;

    public static void main(String[] args) {
        SpringApplication.run(YamlConverterApplication.class, args);
    }

    @Override
    public void run(String... args) throws RuntimeException, IOException {
//        for (int i = 0; i < args.length; ++i) {
//            log.info("args[{}]: {}", i, args[i]);
//        }

        if (args.length != 1) {
            log.error("First parameter have to be input directory, where are located yaml files");
        }

        var inputParam0 = args[0];
        if (!Files.exists(Paths.get(inputParam0))) {
            log.error("Input Path " + inputParam0 + " does not exist");
        }

        if (!Files.isDirectory(Paths.get(inputParam0))) {
            log.error("Input Path " + inputParam0 + " have to be directory");
        }

        //TODO Implement this later
//        if (!Files.exists(Paths.get(args[2]))) {
//            log.error("Output Path " + args[2] + " does not exist");
//        }
        //resolve txt files in directory
        List<Path> txtFiles = resolveFiles.getAllFilesInDirectory(inputParam0);

        //read files one by one and produce yaml to same structure/given directory
        List<InterProcessDto> yamlFiles = txtFiles.stream().map(path -> {
            try {
                return parseFileService.parseFile(path);
            } catch (IOException e) {
                log.error("",e);
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());

        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        om.configure(JsonParser.Feature.ALLOW_YAML_COMMENTS,true);
        // loop thru output structure
        yamlFiles.stream().forEach(interProcessDto -> {
            var newFileString = interProcessDto.getOriginalFilename().substring(0, interProcessDto.getOriginalFilename().indexOf(".txt")) + ".yaml";
//            var newRealFile = Path.of(newFileString);

            // Writing model  into the file in yaml
            try {
                om.writeValue(new File(newFileString), interProcessDto.getOutputFileContent());
                log.info("File was processed: "+ newFileString);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        //bye
    }
}
