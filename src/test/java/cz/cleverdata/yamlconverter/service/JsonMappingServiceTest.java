package cz.cleverdata.yamlconverter.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;



class JsonMappingServiceTest {


    @Test
    void produceJson() throws IOException {
        ResolveFiles resolveFiles = new ResolveFiles();
        JsonMappingService jsonMappingService = new JsonMappingService();
        ExtractJsonService extractJsonService = new ExtractJsonService();

        List<Path> txtFiles = resolveFiles.getAllFilesInDirectory("C:\\Windows\\Temp\\yaml");
        String jsonInString = extractJsonService.produceJsonInString(txtFiles.get(0));

        jsonMappingService.produceJson(jsonInString);
    }
}